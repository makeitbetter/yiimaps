# yiimaps

[![N|Solid](http://makeitbetter.su/images/projects/yiimaps-xs.png)](http://makeitbetter.su/projects/yiimaps)

yiimaps is a module for Yii framework 2.0 to control coordinates of models and displaying the models on map. 
The module uses an [API of Yandex.maps](https://tech.yandex.ru/maps/).

## Installation

Add package:

```
#!sh
$ composer require makeitbetter/yiimaps
```

Configure the application by adding ([yii2 guide](http://www.yiiframework.com/doc-2.0/guide-structure-applications.html#modules)):

```
#!php
$config['modules']['yiimaps'] = [
    'class' => 'makeitbetter\yiimaps\Module',
    'options' => [],
];
$config['bootstrap'][] = 'yiimaps';
```

## Usage

####  In your forms:

```
#!php
use makeitbetter\yiimaps\widgets\CoordsInput;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

echo $form->field($model, 'property_name')->widget(CoordsInput::className(), $options);

ActiveForm::end();
```

Available options:

**lat** - Name of model attribute containing latitude value, default is *lat*.

**lon** - Name of model attribute containing longitude value, default is *lon*.

Or without an *ActiveForm* object:

```
#!php
echo CoordsInput::widget(['model' => $model, 'attribute' => 'property_name']);
```

#### Displaying models on map:

```
#!php
use makeitbetter\yiimaps\widgets\YandexMap;

echo YandexMap::widget([
    'models' => $models,
```
Available options:

**lat** - Name of model attribute containing latitude value, default is *lat*.

**lon** - Name of model attribute containing longitude value, default is *lon*.

**balloonContent** - Name of model attribute or callback function which returns a content for balloon, default is *balloonContent*.

**groupBy** - Name of related entity and its primary key, which contains a coordinates. 
For example, if models *Persons* belongs to entity *Addresses*, you should definde `'groupBy' => 'address.id'`.
Persons with the same address will be displayed on map like a one point. 
Balloon will be contains data of every person and then data of address (except when you use a callback function).

**placemarkIcon** - Icon of placemark, default is *islands#redDotIcon* [see more...](https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml)
If value is an array, keys will be checked against the value of the attribute $placemark.
Use key 'default' to define default placemark icon.

**placemark** - Name of model attribute to define a placemark icon.

**useClusterer** - Wheter to use clusterer, default is *false*.

**clustererIcon** - Clusterer icon, default is *islands#invertedBlackClusterIcons* [see more...](https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml)

**mapHeight** - Map height, default is *300px*.

**mapWidth** - Map width, default is *100%*.

## Internationalisation

For adding translations of module strings register a source `modules/yiimaps`.
For example add to your application config file following code:

```
#!php
$config['components']['i18n'] = [
    'translations' => [
        ...
        'modules*' => [
            'class' => 'yii\i18n\PhpMessageSource',
        ],
    ],
];
```
This configuration means that the application will search the translations at file `@app/messages/xx-XX/modules/yiimaps.php`

See more about internationalisation in [yii2 guide](http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html)

Demo: https://makeitbetter.su/projects/yiimaps
