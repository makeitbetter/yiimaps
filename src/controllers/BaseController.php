<?php

namespace makeitbetter\yiimaps\controllers;

use makeitbetter\yiimaps\models\Folder;
use Exception;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class BaseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->options['roles'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    public function actionIndex()
    {
        $token = base64_decode(Yii::$app->request->get('token'));
        $data = json_decode($token, true);
        return $this->render('index', [
            'id' => Yii::$app->request->get('id'),
            'lat' => Yii::$app->request->get('lat', ''),
            'lon' => Yii::$app->request->get('lon', ''),
        ]);
    }
}
