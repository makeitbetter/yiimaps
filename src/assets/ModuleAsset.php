<?php

namespace makeitbetter\yiimaps\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ModuleAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => View::POS_HEAD];

    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
