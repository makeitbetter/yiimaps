<?php

namespace makeitbetter\yiimaps\widgets;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\base\Widget;

class CoordsInput extends Widget {

    /**
     * Longitude attribute name.
     *
     * @var string;
     */
    public $lon = 'lon';

    /**
     * Latitude attribute name.
     *
     * @var type
     */
    public $lat = 'lat';

    public $model;

    /**
     * @param Model $model
     * @param string $property
     */
    public function run()
    {
        $id = md5($this->model->className() . ',' . $this->lon . ',' . $this->lat);
        Yii::$app->view->registerJs(
<<< JAVASCRIPT
        $(document)
            .on('click', '#$id', function() {
                var query = 'id=$id';
                if ($('#lat$id').val() !== '') {
                    query += '&lat=' + $('#lat$id').val();
                }
                if ($('#lon$id').val() !== '') {
                    query += '&lon=' + $('#lon$id').val();
                }
                if ($(this).val()) {
                    path = $(this).val().split('/');
                    path.pop();
                    path = path.join('/');
                }

                window.open(
                    '/yiimaps/base/index?' + query,
                    'Yiimaps',
                    'width=1024,height=600,resizable=yes,scrollbars=yes,location=no'
                );
            });
JAVASCRIPT
                    );
        $html = '<div class="row form-group"><div class="col-xs-5"><label>' . $this->model->getAttributeLabel($this->lat) . '</label>';
        $options = ['class' => 'form-control', 'id' => "lat$id"];
        $html .= Html::activeTextInput($this->model, $this->lat, $options);
        $html .= '</div><div class="col-xs-5"><label>' . $this->model->getAttributeLabel($this->lon) . '</label>';
        $options['id'] = "lon$id";
        $html .= Html::activeTextInput($this->model, $this->lon, $options);
        $html .= '</div><div class="col-xs-2 text-right"><label>&nbsp;</label><br/>';
        $options['id'] = $id;
        $html .= Html::button('', ['id' => $id, 'class' => 'btn btn-default glyphicon glyphicon-map-marker']);
        $html .= '</div></div>';
        return $html;
    }
}