<?php
/**
 * The widget shows the Yandex Map with points of models on it.
 */

namespace makeitbetter\yiimaps\widgets;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\base\Widget;

class YandexMap extends Widget
{
    /**
     * Models list
     * @var array
     */
    public $models;

    /**
     * Longitude Attribute
     *
     * @var string;
     */
    public $lon = 'lon';

    /**
     * Latitude Attribute
     *
     * @var string
     */
    public $lat = 'lat';

    /**
     * Balloon Content Attribute
     *
     * @var string | function | callback
     */
    public $balloonContent = 'balloonContent';

    /**
     * Group by attribute
     *   Use this option to group models by related entities.
     *   The coordinates should be defined in related entities.
     *   For example:
     * ```
     * $models = [
     *   [
     *     'id' => 1,
     *     'firstname' => 'John',
     *     'lastname' => 'Watson',
     *     'balloonContent' => '<a href="#">John Watson</a>',
     *     'address' => [
     *       'id' => 1,
     *       'line1' => '221B Baker Street',
     *       'line2' => 'London',
     *       'lat' => 51.523746811318915,
     *       'lon' => -0.15859964626163578,
     *       'baloonContent' => '221B Baker Street, London',
     *     ]
     *   ],
     *   [
     *     'id' => 1,
     *     'firstname' => 'Sherlock',
     *     'lastname' => 'Holmes',
     *     'balloonContent' => '<a href="#">Sherlock Holmes</a>',
     *     'address' => [
     *       'id' => 1,
     *       'line1' => '221B Baker Street',
     *       'line2' => 'London',
     *       'lat' => 51.523746811318915,
     *       'lon' => -0.15859964626163578,
     *       'baloonContent' => '221B Baker Street, London',
     *     ]
     *   ],
     * ]
     * ```
     *   Use option 'groupBy' => 'address' (or 'address.id')
     *   These two models will be displayed as one point on map.
     *
     * @var string
     */
    public $groupBy;

    /**
     * Placemark Icon
     *   If value is an array, keys will be checked against the value of the attribute $placemark.
     *   Use key 'default' to define default placemark icon.
     *
     * @var string | array
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml
     */
    public $placemarkIcon = 'islands#redDotIcon';

    /**
     * Placemark Attribute
     *   Attribute which will be used to define placemark icon, if value of $placemarkIcon is an array.
     *
     * @var string
     */
    public $placemark;

    /**
     * Wheter to use clusterer
     *
     * @var bool
     * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer-docpage/
     */
    public $useClusterer = false;

    /**
     * Clusterer Icon
     *
     * @var string
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml
     */
    public $clustererIcon = 'islands#invertedBlackClusterIcons';

    /**
     * Map height
     *
     * @var string
     */
    public $mapHeight = '300px';

    /**
     * Map width
     *
     * @var string
     */
    public $mapWidth = '100%';

    public function run()
    {
        if ($this->groupBy) {
            list($a, $b) = explode('.', $this->groupBy, 2);
            if (!$b) {
                $b = 'id';
            }
            $models = [];
            foreach ($this->models as $model) {
                $models[$model->{$a}->{$b}]['items'][] = $model;
            }
            array_walk($models, function(&$model) use ($a) {
                $model['lat'] = $model['items'][0]->{$a}->{$this->lat};
                $model['lon'] = $model['items'][0]->{$a}->{$this->lon};
                if ($this->placemark) {
                    $model[$this->placemark] = $model['items'][0]->{$this->placemark};
                }
                $model['group'] = $model['items'][0]->{$a};
            });
        }
        else {
            $models = array_map(function($model) {
                $result = [
                    'lat' => $model->{$this->lat},
                    'lon' => $model->{$this->lon},
                    'items' => [$model],
                    'group' => null,
                ];
                if ($this->placemark) {
                    $result[$this->placemark] = $model->{$this->placemark};
                }
                return $result;
            }, (array) $this->models);
        }

        return $this->render('yandex-map', [
            'models' => $models,
            'placemarkIcon' => $this->placemarkIcon,
            'placemark' => $this->placemark,
            'useClusterer' => $this->useClusterer,
            'clustererIcon' => $this->clustererIcon,
            'balloonContent' => $this->balloonContent,
            'mapHeight' => $this->mapHeight,
            'mapWidth' => $this->mapWidth
        ]);
    }
}