<?php

use makeitbetter\yiimaps\assets\ModuleAsset;

ModuleAsset::register($this);

$id = uniqid();

/**
 * @var $this yii\web\View
 * @var $models yii\db\ActiveRecord[]
 */
?>

<div class="yandex-map" id="map<?= $id; ?>" style="width: <?= $mapWidth; ?>; height: <?= $mapHeight; ?>"></div>
<script type="text/javascript">
    var yiiMap;

    ymaps.ready(function(){
        yiiMap = new ymaps.Map('map<?= $id;?>', {
            center: [55, 70],
            zoom: 4,
            controls: ['zoomControl']
        });

        <?php if ($useClusterer) : ?>
        var yiiCollection = new ymaps.Clusterer({
            preset: '<?= $clustererIcon;?>',
            groupByCoordinates: false,
            clusterDisableClickZoom: false,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false
        });

        <?php else : ?>
        var yiiCollection = new ymaps.GeoObjectCollection();
        <?php endif;?>

        <?php foreach ((array) $models as $i => $model) : ?>
        var placemark<?= $i;?> = new ymaps.Placemark(
            [<?= $model['lat'];?>, <?= $model['lon'];?>],
            {
                <?php if (is_callable($balloonContent)) {
                    
                    $content = array_map(function($item) use ($balloonContent) {
                        return str_replace("\n", "\\\n", addslashes(call_user_func($balloonContent, $item)));
                    }, $model['items']);
                    
                    echo 'balloonContent: \'' . implode('', $content) . '\'';
                    
                } elseif ($balloonContent) {
                    $content = array_map(function($item) use ($balloonContent) {
                        if (!$item->hasProperty($balloonContent)) {
                            return '';
                        }
                        return $item->$balloonContent;
                    }, $model['items']);
                    if (!empty($model['group']) && $model['group']->hasProperty($balloonContent)) {
                        $content[] = $model['group']->$balloonContent;
                    }
                    echo 'balloonContent: \'' . implode('', $content) . '\'';
                }?>
            },
            {
                preset: '<?php
                    if (is_array($placemarkIcon)) {
                        echo isset($placemarkIcon[$model[$placemark]]) ? $placemarkIcon[$model[$placemark]] : $placemarkIcon['default'];
                    }
                    else {
                        echo $placemarkIcon;
                    }
                    ?>',
                hideIconOnBalloonOpen: false
            }
        );
        yiiCollection.add(placemark<?= $i;?>);
        <?php endforeach;?>

        yiiMap.geoObjects.add( yiiCollection );
        yiiMap.setBounds( yiiCollection.getBounds() );
        if (yiiMap.getZoom() > 18) {
            yiiMap.setZoom(18);
        }
    });
</script>