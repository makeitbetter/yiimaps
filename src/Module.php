<?php

namespace makeitbetter\yiimaps;

use makeitbetter\yiimaps\helpers\Thumbnail;
use Yii;

/**
 * yiimaps module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'makeitbetter\yiimaps\controllers';

    public $options = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@yiimaps', __DIR__);
        $this->layout = 'index';
        $this->layoutPath = '@yiimaps/layout';

        $defaultOptions = [
            'roles' => ['@'],
        ];
        $this->options = array_replace_recursive($defaultOptions, $this->options);
        setlocale(LC_NUMERIC, 'en_US');
    }

    public static function t($message, $params = [], $language = null)
    {
        try {
            $result = Yii::t('modules/yiimaps', $message, $params, $language);
        } catch (InvalidConfigException $ex ){
            $result = Yii::$app->i18n->format($message, $params, $language);
        }
        return $result;
    }
}
