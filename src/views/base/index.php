<?php

use makeitbetter\yiimaps\Module;
use yii\helpers\Html;

$this->title = 'Yiimaps';
$module = $this->context->module;

$noCoords = $lat === '' || $lon === '';

?>
<p> </p>
<div class="row">
    <div class="col-xs-12 col-sm-3 col-md-2">
        <div class="row">
            <div class="col-xs-6 col-sm-12 form-group">
                <?= Html::button(
                        Module::t('Save'),
                        ['id' => 'save', 'class' => 'btn btn-primary']
                );?>

                <?= Html::button('', [
                    'id' => 'goto-placemark',
                    'class' => 'btn btn-default glyphicon glyphicon-map-marker',
                    'title' => Module::t('Move to the point'),
                    'disabled' => $noCoords
                ]);?>
            </div>
            <div class="col-xs-6 col-sm-12 form-group">
                <?= Html::button(
                    Module::t('Delete the point'),
                    ['id' => 'delete', 'class' => 'btn btn-danger', 'disabled' => $noCoords]
                );?>
            </div>
            <div class="col-xs-6 col-sm-12 form-group">
                <?= Module::t('Lat');?>: <span id="lat"><?= $lat;?></span>
            </div>
            <div class="col-xs-6 col-sm-12 form-group">
                <p><?= Module::t('Lon');?>: <span id="lon"><?= $lon;?></span></p>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-9 col-md-10">
        <div id="map" style="height: 500px;"></div>
    </div>
</div>

<script type="text/javascript">
    var myMap;

ymaps.ready(init);

function init () {

    var center = [<?= $lat === '' ? 55.76 : (float) $lat;?>, <?= $lon === '' ? 37.64 : (float) $lon;?>];

    myMap = new ymaps.Map('map', {
        center: center,
        zoom: 12
    }, {
        searchControlProvider: 'yandex#search'
    });

    myMap.events.add('click', function (e) {
        var coords = e.get('coords');
        setPlacemarkCoords(coords);
    });

    setPlacemarkCoords = function(coords) {
        var point = myMap.geoObjects.get(0);
        if (point) {
            point.geometry.setCoordinates(coords);
        } else {
            createNewPlacemark(coords);
        }
        updateHTML(coords);
    }

    updateHTML = function(coords) {
        document.getElementById('lat').innerHTML = coords[0];
        document.getElementById('lon').innerHTML = coords[1];
    }

    createNewPlacemark = function(coords) {
        var point = new ymaps.Placemark(coords, {
                type: 'Point',
            }, {
                preset: 'islands#redDotIcon',
                draggable: true,
            });
        point.events.add('dragend', function (e) {
            var point = myMap.geoObjects.get(0);
            var coords = point.geometry.getCoordinates();
            updateHTML(coords);
        });
        myMap.geoObjects.add(point);

        document.getElementById('delete').disabled = false;
        document.getElementById('goto-placemark').disabled = false;
    }

    <?php if (!$noCoords) : ?>
    createNewPlacemark([<?= $lat;?>, <?= $lon;?>]);
    <?php endif; ?>

    document.getElementById('delete').onclick = function() {
        myMap.geoObjects.removeAll();
        document.getElementById('delete').disabled = true;
        document.getElementById('goto-placemark').disabled = true;
        document.getElementById('lat').innerHTML = '';
        document.getElementById('lon').innerHTML = '';
    }

    document.getElementById('goto-placemark').onclick = function() {
        var point = myMap.geoObjects.get(0);
        if (point) {
            var coords = point.geometry.getCoordinates();
            myMap.setCenter(coords);
        }
    }
    document.getElementById('save').onclick = function() {
        window.opener.document.getElementById('lat<?= $id;?>').value = document.getElementById('lat').innerHTML;
        window.opener.document.getElementById('lon<?= $id;?>').value = document.getElementById('lon').innerHTML;
        window.close();
    }
}
</script>


